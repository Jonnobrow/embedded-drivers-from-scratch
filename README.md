# Drivers from Scratch

- This is the Git Repository for the from scratch driver development part of the "Mastering Microcontrollers with Peripheral Driver Development" udemy course.
	- [Link to Course](https://www.udemy.com/mastering-microcontroller-with-peripheral-driver-development)
- I followed this course as part of my training for Zircon Software Ltd. during a placement in the summer of 2019.