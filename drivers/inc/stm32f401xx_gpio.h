/*
 * stm32f401xx_gpio.h
 *
 *  Created on: 10 Jul 2019
 *      Author: jonathan.bartlett
 */

#ifndef INC_STM32F401XX_GPIO_H_
#define INC_STM32F401XX_GPIO_H_

#include "stm32f401xx.h"

/*
 * Configuration Structure for GPIO Pin
 */

typedef struct
{
	uint8_t GPIO_PinNumber;
	uint8_t GPIO_PinMode;
	uint8_t GPIO_PinSpeed;
	uint8_t GPIO_PinPuPdControl;
	uint8_t GPIO_PinOPType;
	uint8_t GPIO_PinAltFunMode;
} GPIO_PinConfig_t;


/**
 * Handle Structure for GPIO Pin
 */

typedef struct
{
	GPIO_RegDef_t* pGPIOx; // Base address of the GPIO port to which the pin belongs
	GPIO_PinConfig_t GPIO_PinConfig;
} GPIO_Handle_t;



/*
 * APIS Supported by this driver
 */

void GPIO_Init(GPIO_Handle_t *pGPIOHandle);
void GPIO_DeInit(GPIO_RegDef_t *pGPIOx);

uint8_t GPIO_ReadFromInputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);
uint16_t GPIO_ReadFromInputPort(GPIO_RegDef_t *pGPIOx);
void GPIO_WriteToOutputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t value);
void GPIO_WriteToOutputPort(GPIO_RegDef_t *pGPIOx, uint16_t value);
void GPIO_ToggleOutputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);

void GPIO_PCLK_Control(GPIO_RegDef_t *pGPIOx, uint8_t ENorDI);

void GPIO_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI);
void GPIO_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);
void GPIO_IRQHandling(uint8_t PinNumber);


/*
 * PIN Modes
 */
#define GPIO_MODE_IN		0
#define GPIO_MODE_OUT		1
#define GPIO_MODE_ALTFN		2
#define GPIO_MODE_ANALOG	3
#define GPIO_MODE_IT_FT		4
#define GPIO_MODE_IT_RT		5
#define GPIO_MODE_IT_RFT	6

/*
 * Output Types
 */
#define GPIO_OTYPE_PP		0
#define GPIO_OTYPE_OD		1

/*
 * Output Speeds
 */
#define GPIO_OSPEED_L		0
#define GPIO_OSPEED_M		1
#define GPIO_OSPEED_H		2
#define GPIO_OSPEED_VH		3

/*
 * Pull up pull down mode
 */
#define GPIO_PUPDR_NO		0
#define GPIO_PUPDR_PU		1
#define GPIO_PUPDR_PD		2

/*
 * Pin Numbers
 */
#define GPIO_PIN_NO_0		0
#define GPIO_PIN_NO_1		1
#define GPIO_PIN_NO_2		2
#define GPIO_PIN_NO_3		3
#define GPIO_PIN_NO_4		4
#define GPIO_PIN_NO_5		5
#define GPIO_PIN_NO_6		6
#define GPIO_PIN_NO_7		7
#define GPIO_PIN_NO_8		8
#define GPIO_PIN_NO_9		9
#define GPIO_PIN_NO_10		10
#define GPIO_PIN_NO_11		11
#define GPIO_PIN_NO_12		12
#define GPIO_PIN_NO_13		13
#define GPIO_PIN_NO_14		14
#define GPIO_PIN_NO_15		15

/*
 * EXTIConfig
 */
#define GPIO_EXTI_PORTA_PIN	0
#define GPIO_EXTI_PORTB_PIN	1
#define GPIO_EXTI_PORTC_PIN	2
#define GPIO_EXTI_PORTD_PIN	3
#define GPIO_EXTI_PORTE_PIN	4
#define GPIO_EXTI_PORTH_PIN	7

#define GPIO_ADDR_TO_CODE(x)				((x == GPIOA) ? GPIO_EXTI_PORTA_PIN :\
											(x == GPIOB) ? GPIO_EXTI_PORTB_PIN :\
											(x == GPIOC) ? GPIO_EXTI_PORTC_PIN :\
											(x == GPIOD) ? GPIO_EXTI_PORTD_PIN :\
											(x == GPIOE) ? GPIO_EXTI_PORTE_PIN :\
											(x == GPIOH) ? GPIO_EXTI_PORTH_PIN : 0)

#endif /* INC_STM32F401XX_GPIO_H_ */
