/*
 * stm32f401xx_spi.h
 *
 *  Created on: 11 Jul 2019
 *      Author: jonathan.bartlett
 */

#ifndef INC_STM32F401XX_SPI_H_
#define INC_STM32F401XX_SPI_H_

#include "stm32f401xx.h"


/*
 *	Configuration Structure for SPIx Peripheral
 */
typedef struct
{
	uint8_t SPI_DeviceMode;
	uint8_t SPI_BusConfig;
	uint8_t SPI_SclkSpeed;
	uint8_t SPI_DFF;
	uint8_t SPI_CPOL;
	uint8_t SPI_CPHA;
	uint8_t SPI_SSM;
} SPI_Config_t;

/*
 * Handle Structure for SPIx Peripheral
 */

typedef struct
{
	SPI_RegDef_t	*pSPIx;
	SPI_Config_t	SPIConfig;
	uint8_t			*pTxBuffer;			// Stores the Tx buffer address
	uint8_t			*pRxBuffer;			// Stores the Rx Buffer address
	uint32_t		TxLen;				// Length of Tx Data
	uint32_t		RxLen;				// Length of Rx Data
	uint8_t			TxState;			// Stores the Tx state
	uint8_t			RxState;			// Stores teh Rx state
} SPI_Handle_t;


/**************************************************************************************************************
 * 							APIs Supported By This Driver
 * 				Information about individual APIs can be found in method comments
 *************************************************************************************************************/

/**
 * Peripheral Clock Setup
 * This can be used to enable / disable the peripheral clock
 * @param pSPIx		The SPI Peripheral to enable or disable
 * @param ENorDI	The ENABLE or DISABLE Macro
 */
void SPI_PERIPH_CLK_CONTROL(SPI_RegDef_t *pSPIx, uint8_t ENorDI);

/**
 * Init and De-init
 */
void SPI_INIT(SPI_Handle_t *pSPIHandle);
void SPI_DEINIT(SPI_RegDef_t *pSPIx);

/**
 * Data Send and Receive
 */
void SPI_SEND(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t length);
void SPI_RECEIVE(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t length);
uint8_t SPI_SEND_IT(SPI_Handle_t *pSPIHandle, uint8_t *pTxBuffer, uint32_t length);
uint8_t SPI_RECEIVE_IT(SPI_Handle_t *pSPIHandle, uint8_t *pRxBuffer, uint32_t length);

/**
 * IRQ Configuration and ISR Handling
 */
void SPI_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI);
void SPI_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);
void SPI_IRQHandling(SPI_Handle_t *pHandle);

/*
 * Macros for the SPI Configurables
 */

/**
 * @DEVICE_MODE
 */
#define SPI_DEVICE_MODE_MASTER		1
#define SPI_DEVICE_MODE_SLAVE		0

#define SPI_CR1_MSTR				2

/**
 * @SPI_BUS_CONFIG
 */
#define SPI_BUS_CONFIG_FD			0
#define SPI_BUS_CONFIG_HD			1
#define SPI_BUS_CONFIG_S_TX			2
#define SPI_BUS_CONFIG_S_RX			3

#define SPI_CR1_BIDIMODE			15
#define SPI_CR1_RXONLY				10

/**
 * @SPI_BAUD_RATE
 */
#define SPI_BAUD_PCLK_2				0
#define SPI_BAUD_PCLK_4				1
#define SPI_BAUD_PCLK_8				2
#define SPI_BAUD_PCLK_16			3
#define SPI_BAUD_PCLK_32			4
#define SPI_BAUD_PCLK_64			5
#define SPI_BAUD_PCLK_128			6
#define SPI_BAUD_PCLK_256			7

#define SPI_CR1_BR					3

/**
 * @SPI_DFF
 */
#define SPI_DFF_BITS_8				1
#define SPI_DFF_BITS_16				0

#define SPI_CR1_DFF					11

/**
 * @SPI_CPOL
 */
#define SPI_CPOL_HIGH				1
#define SPI_CPOL_LOW				0

#define SPI_CR1_CPOL				1

/**
 * @SPI_CPHA
 */
#define	SPI_CPHA_HIGH				1
#define SPI_CPHA_LOW				0

#define SPI_CR1_CPHA				0

/**
 * @SPI_SSM
 */
#define SPI_SSM_ENABLE				1
#define SPI_SSM_DISABLE				0

#define SPI_CR1_SSM					9

/**************************************************************************************************************
 * 											Bit Definition Macros for SPI_SR Register
 *************************************************************************************************************/
#define SPI_SR_RXNE					0
#define SPI_SR_TXE					1
#define SPI_SR_CHSIDE				2
#define SPI_SR_UDR					3
#define SPI_SR_CRCERR				4
#define SPI_SR_MODF					5
#define SPI_SR_OVR					6
#define SPI_SR_BSY					7
#define SPI_SR_FRE					8

/**************************************************************************************************************
 * 											Possible Application States Macros
 *************************************************************************************************************/
#define SPI_READY					0
#define SPI_BUSY_RX					1
#define SPI_BUSY_TX					2

#define SPI_EVENT_TX_DONE			1
#define SPI_EVENT_RX_DONE			2
#define SPI_EVENT_OVR_ERR			3

#endif /* INC_STM32F401XX_SPI_H_ */
