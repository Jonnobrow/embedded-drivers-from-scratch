/*
 * stm32f401xx_i2c.h
 *
 *  Created on: 12 Jul 2019
 *      Author: jonathan.bartlett
 */

#ifndef INC_STM32F401XX_I2C_H_
#define INC_STM32F401XX_I2C_H_

#include "stm32f401xx.h"

/**************************************************************************************************************
 * 										I2C Handle and Configuration Structure
 *************************************************************************************************************/
typedef struct {
	uint32_t	I2C_SCLSpeed;
	uint8_t		I2C_DeviceAddress;
	uint8_t		I2C_AckControl;
	uint8_t		I2C_FMDutyCycle;
} I2C_Config_t;

typedef struct {
	I2C_RegDef_t *pI2Cx;
	I2C_Config_t I2C_Config;
} I2C_Handle_t;

/**************************************************************************************************************
 * 												Options Macros
 *************************************************************************************************************/

/*
 * @I2C_SCLSpeed
 */
#define I2C_SCL_SPEED_SM	100000
#define I2C_SCL_SPEED_FM2	200000
#define I2C_SCL_SPEED_FM4	400000

/*
 * @I2C_AckControl
 */
#define I2C_ACK_ENABLE		1
#define I2C_ACK_DISABLE		0

/*
 * @I2C_FMDutyCycle
 */
#define I2C_FMDUTY_2		0
#define I2C_FMDUTY_16_9		1

/**************************************************************************************************************
 *												API Prototypes
 *************************************************************************************************************/
/**
 * Peripheral Clock Setup
 */
void I2C_PERIPH_CLK_CONTROL(I2C_RegDef_t *pI2Cx, uint8_t ENorDI);

/**
 * Init and De-init
 */
void I2C_INIT(I2C_Handle_t *pI2C_Handle);
void I2C_DEINIT(I2C_RegDef_t *pI2Cx);

/**
 * Send and Receive
 */
void I2C_MasterSendData(I2C_Handle_t *pI2CHandle, uint8_t *pTxBuffer, uint32_t length, uint8_t slaveAddr);

/**
 * IRQ Configuration and ISR Handling
 */
void I2C_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI);
void I2C_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);

/**
 * Other Peripheral Control APIs
 */
void I2C_PeripheralControl(I2C_RegDef_t *pI2Cx, uint8_t ENorDI);
uint8_t I2C_GetFlagStatus(I2C_RegDef_t *pI2Cx, uint32_t FlagName);

/**
 * Application Callback
 */
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandler, uint8_t Event);

/**************************************************************************************************************
 * 													Flags
 *************************************************************************************************************/
#define I2C_TXE_FLAG	(1 << 7)
#define I2C_RXNE_FLAG	(1 << 6)
#define I2C_STOPF_FLAG 	(1 << 4)
#define I2C_BTF_FLAG	(1 << 2)
#define I2C_ADDR_FLAG	(1 << 1)
#define I2C_SB_FLAG		(1 << 0)

#endif /* INC_STM32F401XX_I2C_H_ */
