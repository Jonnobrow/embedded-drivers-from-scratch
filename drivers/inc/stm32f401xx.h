/*
 * stm32f401xx.h
 *
 *  Created on: 9 Jul 2019
 *      Author: jonathan.bartlett
 */

#ifndef INC_STM32F401XX_H_
#define INC_STM32F401XX_H_

#include <stdint.h>
#include <stddef.h>

/*
 * Volatile Shorthand
 */
#define _vo	volatile

/*
 * Processor Specific Details
 */
#define NVIC_ISER0							(_vo uint32_t*)0xE000E100
#define NVIC_ISER1							(_vo uint32_t*)0xE000E104
#define NVIC_ISER2							(_vo uint32_t*)0xE000E108
#define NVIC_ICER0							(_vo uint32_t*)0xE000E180
#define NVIC_ICER1							(_vo uint32_t*)0xE000E184
#define NVIC_ICER2							(_vo uint32_t*)0xE000E188
#define NVIC_PRBASE							(_vo uint32_t*)0xE000E400
#define NUM_PR_BIT_IMPLEMENTED				4
/*
 * Base addresses of flash and sram
 */

#define FLASH_BASE							0x0800000U			/* The base address of flash memory 		*/
#define SRAM1_BASE							0x2000000U			/* The base address of SRAM1	    		*/
#define SRAM								SRAM1_BASE			/* The base address of SRAM					*/
#define ROM_BASE							0x1FFF0000U			/* The base address of the System Memory 	*/

/*
 * Base addresses of bus domains
 */

#define APB1PERIPH_BASE						0x40000000U
#define APB2PERIPH_BASE						0x40010000U
#define AHB1PERIPH_BASE						0x40020000U
#define AHB2PERIPH_BASE						0x50000000U

/*
 * Base addresses for each AHB1 Peripheral
 */

#define CRC_BASE							(AHB1PERIPH_BASE + 0x3000)
#define DMA1_BASE							(AHB1PERIPH_BASE + 0x6000)
#define DMA2_BASE							(AHB1PERIPH_BASE + 0x6400)
#define FlashInterfaceRegister_BASE			(AHB1PERIPH_BASE + 0x3C00)
#define GPIOA_BASE							(AHB1PERIPH_BASE + 0x0000)
#define GPIOB_BASE							(AHB1PERIPH_BASE + 0x0400)
#define GPIOC_BASE							(AHB1PERIPH_BASE + 0x0800)
#define GPIOD_BASE							(AHB1PERIPH_BASE + 0x0C00)
#define GPIOE_BASE							(AHB1PERIPH_BASE + 0x1000)
#define GPIOH_BASE							(AHB1PERIPH_BASE + 0x1C00)
#define RCC_BASE							(AHB1PERIPH_BASE + 0x3800)

/*
 * Base addresses for each APB1 Peripheral
 */

#define I2C1_BASE							(APB1PERIPH_BASE + 0x5400)
#define I2C2_BASE							(APB1PERIPH_BASE + 0x5800)
#define I2C3_BASE							(APB1PERIPH_BASE + 0x5C00)
#define I2S2EXT_BASE						(APB1PERIPH_BASE + 0x3400)
#define I2S3EXT_BASE						(APB1PERIPH_BASE + 0x4000)
#define IWDG_BASE							(APB1PERIPH_BASE + 0x3000)
#define PWR_BASE							(APB1PERIPH_BASE + 0x7000)
#define RTC_BASE							(APB1PERIPH_BASE + 0x2800)
#define SPI2_BASE							(APB1PERIPH_BASE + 0x3800)
#define SPI3_BASE							(APB1PERIPH_BASE + 0x3C00)
#define TIM2_BASE							APB1PERIPH_BASE
#define TIM3_BASE							(APB1PERIPH_BASE + 0x0400)
#define TIM4_BASE							(APB1PERIPH_BASE + 0x0800)
#define TIM5_BASE							(APB1PERIPH_BASE + 0x0C00)
#define USART2_BASE							(APB1PERIPH_BASE + 0x4400)
#define WWDG_BASE							(APB1PERIPH_BASE + 0x2C00)

/*
 * Base address for each APB2 Peripheral
 */

#define ADC1_BASE							(APB2PERIPH_BASE + 0x2000)
#define EXTI_BASE							(APB2PERIPH_BASE + 0x3C00)
#define SDIO_BASE							(APB2PERIPH_BASE + 0x2C00)
#define SPI1_BASE							(APB2PERIPH_BASE + 0x3000)
#define SPI4_BASE							(APB2PERIPH_BASE + 0x3400)
#define SYSCFG_BASE							(APB2PERIPH_BASE + 0x3800)
#define TIM1_BASE							(APB2PERIPH_BASE)
#define TIM10_BASE							(APB2PERIPH_BASE + 0x4400)
#define TIM11_BASE							(APB2PERIPH_BASE + 0x4800)
#define TIM9_BASE							(APB2PERIPH_BASE + 0x4000)
#define USART1_BASE							(APB2PERIPH_BASE + 0x1000)
#define USART6_BASE							(APB2PERIPH_BASE + 0x1400)

/*
 * Peripheral Register Definition Structure CRC
 */

typedef struct
{
	uint32_t DR;
	uint32_t IDR;
	uint32_t CR;
} CRC_RegDef_t;

#define CRC									((*CRC_RegDef_t) CRC_BASE)

/*
 * Peripheral Register Definition Structure for DMA
 */

typedef struct
{
	uint32_t LISR;
	uint32_t HISR;
	uint32_t LIFCR;
	uint32_t HIFCR;
	uint32_t SOCR;
	uint32_t SONDTR;
	uint32_t SOPAR;
	uint32_t SOMOAR;
	uint32_t SOM1AR;
	uint32_t SOFCR;
	uint32_t S1CR;
	uint32_t S1NDTR;
	uint32_t S1PAR;
	uint32_t S1M0AR;
	uint32_t S1M1AR;
	uint32_t S1FCR;
	uint32_t S2CR;
	uint32_t S2NDTR;
	uint32_t S2PAR;
	uint32_t S2MOAR;
	uint32_t S2M1AR;
	uint32_t S2FCR;
	uint32_t S3CR;
	uint32_t S3NDTR;
	uint32_t S3PAR;
	uint32_t S3MOAR;
	uint32_t SSM1AR;
	uint32_t S3FCR;
	uint32_t S4CR;
	uint32_t S4NDTR;
	uint32_t S4PAR;
	uint32_t S4MOAR;
	uint32_t S4M1AR;
	uint32_t S4FCR;
	uint32_t S5CR;
	uint32_t S5NDTR;
	uint32_t S5PAR;
	uint32_t S5MOAR;
	uint32_t S5M1AR;
	uint32_t S5FCR;
	uint32_t S6CR;
	uint32_t S6NDTR;
	uint32_t S6PAR;
	uint32_t S6MOAR;
	uint32_t S6M1AR;
	uint32_t S6FCR;
	uint32_t S7CR;
	uint32_t S7NDTR;
	uint32_t S7PAR;
	uint32_t S7MOAR;
	uint32_t S7M1AR;
	uint32_t S7FCR;
} DMA_RegDef_t;

#define DMA1								((DMA_RegDef_t*) DMA1_BASE)
#define DMA2								((DMA_RegDef_t*) DMA2_BASE)

/*
 * Peripheral Register Definition Structure for Flash Interface
 */

typedef struct
{
	uint32_t ACR;
	uint32_t KEYR;
	uint32_t OPTKEYR;
	uint32_t SR;
	uint32_t CR;
	uint32_t OPTCR;
} FlashInterface_RegDef_t;

#define FlashInterface						((FlashInterface_RegDef_t*) FlashInterfaceRegister_BASE)

/*
 * Peripheral Register Definition Structure for GPIO
 */
typedef struct
{
	uint32_t MODER;
	uint32_t OTYPER;
	uint32_t OSPEEDR;
	uint32_t PUPDR;
	uint32_t IDR;
	uint32_t ODR;
	uint32_t BSRR;
	uint32_t LCKR;
	uint32_t AFR[2];
} GPIO_RegDef_t;

#define GPIOA 								((GPIO_RegDef_t*) GPIOA_BASE)
#define GPIOB 								((GPIO_RegDef_t*) GPIOB_BASE)
#define GPIOC 								((GPIO_RegDef_t*) GPIOC_BASE)
#define GPIOD 								((GPIO_RegDef_t*) GPIOD_BASE)
#define GPIOE 								((GPIO_RegDef_t*) GPIOE_BASE)
#define GPIOH 								((GPIO_RegDef_t*) GPIOH_BASE)

/*
 * Peripheral Register Definition Structure for RCC
 */

typedef struct
{
	_vo uint32_t CR;
	_vo uint32_t PLLCFGR;
	_vo uint32_t CFGR;
	_vo uint32_t CIR;
	_vo uint32_t AHB1RSTR;
	_vo uint32_t AHB2RSTR;
	uint32_t reserved1[2];
	_vo uint32_t APB1RSTR;
	_vo uint32_t APB2RSTR;
	uint32_t reserved2[2];
	_vo uint32_t AHB1ENR;
	_vo uint32_t AHB2ENR;
	uint32_t reserved3[2];
	_vo uint32_t APB1ENR;
	_vo uint32_t APB2ENR;
	uint32_t reserved4[2];
	_vo uint32_t AHB1LPENR;
	_vo uint32_t AHB2LPENR;
	uint32_t reserved5[2];
	_vo uint32_t APB1LPENR;
	_vo uint32_t APB2LPENR;
	uint32_t reserved6[2];
	_vo uint32_t BDCR;
	_vo uint32_t CSR;
	uint32_t reserved7[2];
	_vo uint32_t SSCGR;
	_vo uint32_t PLLI2SCFGR;
	_vo uint32_t reserved8;
	_vo uint32_t DCKCFGR;
} RCC_RegDef_t;

#define RCC									((RCC_RegDef_t*) RCC_BASE)

/*
 * Peripheral Register Definition Structure for I2C
 */

typedef struct
{
	uint32_t CR1;
	uint32_t CR2;
	uint32_t OAR1;
	uint32_t OAR2;
	uint32_t DR;
	uint32_t SR1;
	uint32_t SR2;
	uint32_t CCR;
	uint32_t TRISE;
	uint32_t FLTR;
} I2C_RegDef_t;

#define I2C1								((I2C_RegDef_t*) I2C1_BASE)
#define I2C2								((I2C_RegDef_t*) I2C2_BASE)
#define I2C3								((I2C_RegDef_t*) I2C3_BASE)

/*
 * Peripheral Register Definition Structure for EXTI
 */

typedef struct
{
	_vo uint32_t IMR;
	_vo uint32_t EMR;
	_vo uint32_t RTSR;
	_vo uint32_t FTSR;
	_vo uint32_t SWIER;
	_vo uint32_t PR;
} EXTI_RegDef_t;

#define EXTI								((EXTI_RegDef_t*) EXTI_BASE)

/*
 * Peripheral Register Definition Structure of SYSCFG
 */

typedef struct
{
	_vo uint32_t MEMRMP;
	_vo uint32_t PMC;
	_vo uint32_t EXTICR[4];
	uint32_t RESERVED[2];
	_vo uint32_t CMPCR;
} SYSCFG_RegDef_t;

#define SYSCFG								((SYSCFG_RegDef_t*) SYSCFG_BASE)

/*
 * Peripheral Register Definition Structure for SPIx
 */

typedef struct
{
	_vo uint32_t CR1;
	_vo uint32_t CR2;
	_vo uint32_t SR;
	_vo uint32_t DR;
	_vo uint32_t CRCPR;
	_vo uint32_t RXCRCR;
	_vo uint32_t TXCRCR;
	_vo uint32_t I2SCFGR;
	_vo uint32_t I2SPR;
} SPI_RegDef_t ;

#define SPI1								((SPI_RegDef_t*) SPI1_BASE)
#define SPI2								((SPI_RegDef_t*) SPI2_BASE)
#define SPI3								((SPI_RegDef_t*) SPI3_BASE)
#define SPI4								((SPI_RegDef_t*) SPI4_BASE)

/*
 * Clock Enable Macros for GPIOx Peripherals
 */

#define GPIOA_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 0 ))
#define GPIOB_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 1 ))
#define GPIOC_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 2 ))
#define GPIOD_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 3 ))
#define GPIOE_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 4 ))
#define GPIOH_PCLCK_EN()		(RCC->AHB1ENR |= ( 1 << 7 ))

/*
 * Clock Enable Macros for I2Cx Peripherals
 */

#define I2C1_PCLK_EN()			(RCC->APB1ENR |= ( 1 << 21 ))
#define I2C2_PCLK_EN()			(RCC->APB1ENR |= ( 1 << 22 ))
#define I2C3_PCLK_EN()			(RCC->APB1ENR |= ( 1 << 23 ))

/*
 * Clock Enable Macros for SPIx Peripherals
 */

#define SPI1_PCLK_EN()			(RCC->APB2ENR |= ( 1 << 12))
#define SPI2_PCLK_EN()			(RCC->APB1ENR |= ( 1 << 14))
#define SPI3_PCLK_EN()			(RCC->APB1ENR |= ( 1 << 15))
#define SPI4_PCLK_EN()			(RCC->APB2ENR |= ( 1 << 13))

/*
 * Clock Enable Macros for USARTx Peripherals
 */

#define USART1_PCLK_EN()		(RCC->APB2ENR |= (1 << 4))
#define USART2_PCLK_EN()		(RCC->APB1ENR |= (1 << 17))
#define USART6_PCLK_EN()		(RCC->APB2ENR |= (1 << 5))

/*
 * Clock Enable Macros for SYSCFG Peripheral
 */

#define SYSCFG_PCLK_EN()		(RCC->APB2ENR |= (1 << 14))

/*
 * Clock Disable Macros for GPIOx Peripherals
 */

#define GPIOA_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 0 ))
#define GPIOB_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 1 ))
#define GPIOC_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 2 ))
#define GPIOD_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 3 ))
#define GPIOE_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 4 ))
#define GPIOH_PCLCK_DI()		(RCC->AHB1ENR &= ~( 1 << 7 ))

/*
 * Clock Disable Macros for I2Cx Peripherals
 */

#define I2C1_PCLK_DI()			(RCC->APB1ENR &= ~( 1 << 21 ))
#define I2C2_PCLK_DI()			(RCC->APB1ENR &= ~( 1 << 22 ))
#define I2C3_PCLK_DI()			(RCC->APB1ENR &= ~( 1 << 23 ))

/*
 * Clock Disable Macros for SPIx Peripherals
 */

#define SPI1_PCLK_DI()			(RCC->APB2ENR &= ~( 1 << 12))
#define SPI2_PCLK_DI()			(RCC->APB1ENR &= ~( 1 << 14))
#define SPI3_PCLK_DI()			(RCC->APB1ENR &= ~( 1 << 15))
#define SPI4_PCLK_DI()			(RCC->APB2ENR &= ~( 1 << 13))

/*
 * Clock Disable Macros for USARTx Peripherals
 */

#define USART1_PCLK_DI()		(RCC->APB2ENR &= ~(1 << 4))
#define USART2_PCLK_DI()		(RCC->APB1ENR &= ~(1 << 17))
#define USART6_PCLK_DI()		(RCC->APB2ENR &= ~(1 << 5))

/*
 * Clock Disable Macros for SYSCFG Peripheral
 */

#define SYSCFG_PCLK_DI()		(RCC->APB2ENR &= ~(1 << 14))

/*
 * Macros to reset GPIOx Peripherals
 */
#define GPIOA_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<0)); (RCC->AHB1RSTR &= ~(1<<0));} while(0)
#define GPIOB_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<1)); (RCC->AHB1RSTR &= ~(1<<1));} while(0)
#define GPIOC_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<2)); (RCC->AHB1RSTR &= ~(1<<2));} while(0)
#define GPIOD_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<3)); (RCC->AHB1RSTR &= ~(1<<3));} while(0)
#define GPIOE_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<4)); (RCC->AHB1RSTR &= ~(1<<4));} while(0)
#define GPIOH_REG_RESET()		do{ (RCC->AHB1RSTR |= (1<<7)); (RCC->AHB1RSTR &= ~(1<<7));} while(0)

/*
 * IRQ Values for EXTI Lines
 */
#define IRQ_NO_EXTI0			6
#define IRQ_NO_EXTI1			7
#define IRQ_NO_EXTI2			8
#define IRQ_NO_EXTI3			9
#define IRQ_NO_EXTI4			10
#define IRQ_NO_EXTI9_5			23
#define IRQ_NO_EXTI15_10		40

/*
 * IRQ Values for SPI Lines
 */
#define IRQ_NO_SPI1				35
#define IRQ_NO_SPI2				36
#define IRQ_NO_SPI3				51
#define IRQ_NO_SPI4				84


/*
 * Generic Macros
 */
#define ENABLE 		1
#define DISABLE 	0


/**************************************************************************************************************
 * 													IRQ Methods
 *************************************************************************************************************/

void IRQConfig(uint8_t IRQNumber, uint8_t ENorDI);
void IRQPriorityConfig(uint8_t IRQNumber, uint8_t IRQPriority);

#endif /* INC_STM32F401XX_H_ */
