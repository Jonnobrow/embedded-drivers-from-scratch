/*
 * stm32f401xx_i2c.c
 *
 *  Created on: 12 Jul 2019
 *      Author: jonathan.bartlett
 */

#include "stm32f401xx.h"
#include "stm32f401xx_i2c.h"

/**
 * Peripheral Clock Setup
 */
void I2C_PERIPH_CLK_CONTROL(I2C_RegDef_t *pI2Cx, uint8_t ENorDI)
{
	if (ENorDI == ENABLE) {
		if (pI2Cx == I2C1) {
			I2C1_PCLK_EN();
		} else if (pI2Cx == I2C2) {
			I2C2_PCLK_EN();
		} else if (pI2Cx == I2C3) {
			I2C3_PCLK_EN();
		}
	} else {
		if (pI2Cx == I2C1) {
			I2C1_PCLK_DI();
		} else if (pI2Cx == I2C2) {
			I2C2_PCLK_DI();
		} else if (pI2Cx == I2C3) {
			I2C3_PCLK_DI();
		}
	}
}

uint32_t RCC_GetPLLOutputClock() {
	return 0;
};

uint16_t AHB_PreScaler[8] = {2,4,8,16,64,128,256,512};
uint16_t APB_PreScaler[4] = {2,4,8,16};

uint32_t RCC_GetPCLK1Value() {
	// Find which system clock is used
	uint8_t SYSCLK_SRC = (RCC->CFGR >> 2) & 0x3;
	uint8_t AHBP,APB1P;
	uint32_t PCLK1, SYSCLK;

	if (SYSCLK_SRC == 0) {
		SYSCLK = 16000000;
	} else if (SYSCLK_SRC == 1) {
		SYSCLK = 8000000;
	} else if (SYSCLK_SRC == 2) {
		SYSCLK = RCC_GetPLLOutputClock();
	}

	uint8_t temp = ((RCC->CFGR >> 4) & 0xF);
	if (temp < 8)
	{
		AHBP = 1;
	} else {
		AHBP = AHB_PreScaler[temp-8];
	}

	temp = ((RCC->CFGR >> 10) & 0x7);
	if (temp < 4) {
		APB1P = 1;
	} else {
		APB1P = APB_PreScaler[temp-4];
	}

	PCLK1 = (SYSCLK / AHBP) / APB1P;

	return PCLK1;
}

/**
 * Init and De-init
 */
void I2C_INIT(I2C_Handle_t *pI2C_Handle)
{
	uint32_t tempReg = 0;

	// Ack Control
	tempReg |= pI2C_Handle->I2C_Config.I2C_AckControl << 10;
	pI2C_Handle->pI2Cx->CR1 = tempReg;

	// PCLK1 Freq into CR2
	tempReg = 0;
	tempReg |= RCC_GetPCLK1Value() / 1000000U;
	pI2C_Handle->pI2Cx->CR2 = tempReg;

	// OAR1 Slave Address
	tempReg = 0;
	tempReg |= pI2C_Handle->I2C_Config.I2C_DeviceAddress;
	tempReg |= (1 << 14);
	pI2C_Handle->pI2Cx->OAR1=tempReg;

	// CCR Register
	tempReg = 0;
	uint16_t ccr_value = 0;
	if (pI2C_Handle->I2C_Config.I2C_SCLSpeed <= I2C_SCL_SPEED_SM) {
		// Standard Mode
		ccr_value = RCC_GetPCLK1Value() / (2 * pI2C_Handle->I2C_Config.I2C_SCLSpeed);
		tempReg |= (ccr_value & 0xFFF);
	} else {
		// Fast Mode
		tempReg |= (1 << 15);
		tempReg |= (pI2C_Handle->I2C_Config.I2C_FMDutyCycle << 14);
		if (pI2C_Handle->I2C_Config.I2C_FMDutyCycle == I2C_FMDUTY_2) {
			ccr_value = RCC_GetPCLK1Value() / (3 * pI2C_Handle->I2C_Config.I2C_SCLSpeed);
		} else {
			ccr_value = RCC_GetPCLK1Value() / (25 * pI2C_Handle->I2C_Config.I2C_SCLSpeed);
		}
		tempReg |= (ccr_value & 0xFFF);
	}
	pI2C_Handle->pI2Cx->CCR = tempReg;
	// Set up TRISE
	tempReg = 0;
	if (pI2C_Handle->I2C_Config.I2C_SCLSpeed <= I2C_SCL_SPEED_SM) {
		tempReg = ((RCC_GetPCLK1Value() / 1000000U) + 1);
	} else {
		tempReg = ((RCC_GetPCLK1Value() * 300) / 1000000000U) + 1;
	}
	pI2C_Handle->pI2Cx->TRISE = tempReg & 0x3F;

	// Enable the Clock
	I2C_PERIPH_CLK_CONTROL(pI2C_Handle->pI2Cx, ENABLE);

}

void I2C_DEINIT(I2C_RegDef_t *pI2Cx) {
	if (pI2Cx == I2C1) {
		RCC->APB1RSTR |= (1 << 21);
	} else if (pI2Cx == I2C2) {
		RCC->APB1RSTR |= (1 << 22);
	} else if (pI2Cx == I2C3) {
		RCC->APB1RSTR |= (1 << 23);
	}
}

/**
 * Send and Receive
 */

static void I2C_GenerateStartCondition(I2C_RegDef_t *pI2Cx) {
	pI2Cx->CR1 |= ( 1 << 8 ) ;
}

static void I2C_GenerateStopCondition(I2C_RegDef_t *pI2Cx) {
	pI2Cx->CR1 |= ( 1 << 7 ) ;
}

static void I2C_ClearADDRFlag(I2C_RegDef_t *pI2Cx) {
	uint32_t dummyRead = pI2Cx->SR1;
	dummyRead = pI2Cx->SR2;
	(void)dummyRead;
}

void I2C_MasterSendData(I2C_Handle_t *pI2CHandle, uint8_t *pTxBuffer, uint32_t length, uint8_t slaveAddr) {
	// Generate the start condition
	I2C_GenerateStartCondition(pI2CHandle->pI2Cx);
	// Get the SB Flag
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_SB_FLAG));
	// Send the address of the slave
	slaveAddr = slaveAddr << 1;
	slaveAddr &= ~(1);
	pI2CHandle->pI2Cx->DR = slaveAddr;
	// Check address phase done
	while (!I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_ADDR_FLAG));
	I2C_ClearADDRFlag(pI2CHandle->pI2Cx);
	// Send Data
	while (length > 0) {
		while (! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_TXE_FLAG));
		pI2CHandle->pI2Cx->DR = *pTxBuffer;
		pTxBuffer++;
		length--;
	}
	// Wait for TXE=1 and BTF=1 before stop condition
	while (! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_TXE_FLAG));
	while (! I2C_GetFlagStatus(pI2CHandle->pI2Cx, I2C_BTF_FLAG));
	I2C_GenerateStopCondition(pI2CHandle->pI2Cx);

}


/**
 * IRQ Configuration and ISR Handling
 */
void I2C_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI) {
	IRQConfig(IRQNumber, ENorDI);
}

void I2C_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority) {
	IRQPriorityConfig(IRQNumber, IRQPriority);
}

/**
 * Other Peripheral Control APIs
 */
void I2C_PeripheralControl(I2C_RegDef_t *pI2Cx, uint8_t ENorDI) {
	if (ENorDI == ENABLE) {
		pI2Cx->CR1 |= (1 << 0);
	} else {
		pI2Cx->CR1 &= ~(1 << 0);
	}
}

uint8_t I2C_GetFlagStatus(I2C_RegDef_t *pI2Cx, uint32_t FlagName) {
	if (pI2Cx->SR1 & FlagName)
		return 1;
	return 0;
}

/**
 * Application Callback
 */
void I2C_ApplicationEventCallback(I2C_Handle_t *pI2CHandler, uint8_t Event) {

}
