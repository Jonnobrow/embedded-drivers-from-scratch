/*
 * stm32f401xx_spi.c
 *
 *  Created on: 11 Jul 2019
 *      Author: jonathan.bartlett
 */

#include "stm32f401xx_spi.h"

/**************************************************************************************************************
 * 										API Methods
 *************************************************************************************************************/

/**
 * @SPI_PERIPH_CLK_CONTROL
 *
 * Enables or Disable the Peripheral Clock for a Specified SPIx Peripheral
 *
 * @param pSPIx			The SPI Peripheral To Enable or Disable
 * @param ENorDI		The ENABLE or DISABLE Macro
 */
void SPI_PERIPH_CLK_CONTROL(SPI_RegDef_t *pSPIx, uint8_t ENorDI)
{
	if (ENorDI == ENABLE) {
		if (pSPIx == SPI1) {
			SPI1_PCLK_EN();
		} else if (pSPIx == SPI2) {
			SPI2_PCLK_EN();
		} else if (pSPIx == SPI3) {
			SPI3_PCLK_EN();
		} else if (pSPIx == SPI4) {
			SPI4_PCLK_EN();
		}
	} else {
		if (pSPIx == SPI1) {
			SPI1_PCLK_DI();
		} else if (pSPIx == SPI2) {
			SPI2_PCLK_DI();
		} else if (pSPIx == SPI3) {
			SPI3_PCLK_DI();
		} else if (pSPIx == SPI4) {
			SPI4_PCLK_DI();
		}
	}
}

/**
 * @SPI_INIT
 *
 * Initialises a SPI Peripheral
 *
 * @param pSPIHandle	The Handle Data Structure for the SPI Peripheral
 */
void SPI_INIT(SPI_Handle_t *pSPIHandle)
{
	SPI_PERIPH_CLK_CONTROL(pSPIHandle->pSPIx, ENABLE);
	pSPIHandle->pSPIx->CR1 = 0;

	// Master / Slave Config
	if (pSPIHandle->SPIConfig.SPI_DeviceMode == SPI_DEVICE_MODE_MASTER) {
		pSPIHandle->pSPIx->CR1 |= (1 << SPI_CR1_MSTR);
	} else {
		pSPIHandle->pSPIx->CR1 &= ~(1 << SPI_CR1_MSTR);
	}

	// Buffer Config
	if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_FD) {
		// Clears the BIDIMODE Bit to Enable Full Duplex
		pSPIHandle->pSPIx->CR1 &= ~(1 << SPI_CR1_BIDIMODE);
	} else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_HD) {
		// Sets the BIDIMODE Bit to Enable Half Duplex
		pSPIHandle->pSPIx->CR1 |= (1 << SPI_CR1_BIDIMODE);
	} else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_S_RX) {
		// Clearly the BIDIMODE Bit and Sets the RXONLY Bit
		pSPIHandle->pSPIx->CR1 &= ~(1 << SPI_CR1_BIDIMODE);
		pSPIHandle->pSPIx->CR1 |= (1 << SPI_CR1_RXONLY);
	}

	// Phase and Polarity
	pSPIHandle->pSPIx->CR1 |= (pSPIHandle->SPIConfig.SPI_CPOL << SPI_CR1_CPOL);
	pSPIHandle->pSPIx->CR1 |= (pSPIHandle->SPIConfig.SPI_CPHA << SPI_CR1_CPHA);

	// SSM
	pSPIHandle->pSPIx->CR1 |= (pSPIHandle->SPIConfig.SPI_SSM << SPI_CR1_SSM);

	// DFF
	pSPIHandle->pSPIx->CR1 |= (pSPIHandle->SPIConfig.SPI_DFF << SPI_CR1_DFF);

	// Baud Rate
	pSPIHandle->pSPIx->CR1 |= (pSPIHandle->SPIConfig.SPI_SclkSpeed << SPI_CR1_BR);
}

/**
 * @SPI_DEINIT
 *
 * De-Initialises an SPI Peripheral
 *
 * @param pSPIx			The Base address of the SPI Peripheral to De-Initialise
 */
void SPI_DEINIT(SPI_RegDef_t *pSPIx)
{
	if (pSPIx == SPI1) {
		RCC->APB2RSTR |= (1 << 12);
	} else if (pSPIx == SPI2) {
		RCC->APB1RSTR |= (1 << 14);
	} else if (pSPIx == SPI3) {
		RCC->APB1RSTR |= (1 << 15);
	} else if (pSPIx == SPI4) {
		RCC->APB2RSTR |= (1 << 13);
	}
}


uint8_t SPI_GetSRFlagStatus(SPI_RegDef_t *pSPIx, uint8_t FlagName) {
	return pSPIx->SR & (1 << FlagName );
}


/**
 * @SPI_SEND
 *
 * Sends Data over a specified SPIx Peripheral
 *
 * @param pSPIx			The base address of the SPI Peripheral to send over
 * @param pTxBuffer		The transaction buffer location (where is the data)
 * @param length		The length of the data to send
 */
void SPI_SEND(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t length)
{
	while(length > 0)
	{
		// Hang while the TxBuffer is not empty
		while(SPI_GetSRFlagStatus(pSPIx, SPI_SR_TXE) == 0);

		// Write to Data Register
		if (pSPIx->CR1 & ( 1 << SPI_CR1_DFF) ) {
			// Assigns 2 Bits to DR
			pSPIx->DR = *((uint16_t*) pTxBuffer);
			length --;
			length --;
			(uint16_t*)pTxBuffer++;
		} else {
			// Assigns 1 Bit to DR
			pSPIx->DR = *(pTxBuffer);
			length --;
			pTxBuffer++;
		}

	}
}

/**
 * @SPI_RECEIVE
 *
 * Receives Data from a specified SPIx Peripheral
 *
 * @param pSPIx			The base address of the SPI Peripheral to receive from
 * @param pRxBuffer		The receive buffer location (where is the data)
 * @param length		The length of the data to send
 */
void SPI_RECEIVE(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t length)
{
	while (length > 0)
	{
		// Wait for the RxBuffer to become non-empty
		while(SPI_GetSRFlagStatus(pSPIx, SPI_SR_RXNE) == 0);

		// Check the DFF
		if(pSPIx->CR1 & (1 << SPI_CR1_DFF)) {
			*((uint16_t*)pRxBuffer) = pSPIx->DR;
			length--;
			length--;
			(uint16_t*)pRxBuffer++;
		} else {
			*(pRxBuffer) = pSPIx->DR;
			length--;
			pRxBuffer++;
		}
	}
}

/**
 * @SPI_SEND_IT
 *
 * Interrupt Enabled SPI SEND Method
 *
 * @param pSPIHandle 		The handle for the SPI port to use
 * @param pTxBuffer			The location of the data to send
 * @param length			The length of the data to send
 */
uint8_t SPI_SEND_IT(SPI_Handle_t *pSPIHandle, uint8_t *pTxBuffer, uint32_t length)
{
	if (pSPIHandle->TxState == SPI_READY) {
		// Save the Tx Buffer Address and Len Information in some variables
		pSPIHandle->pTxBuffer = pTxBuffer;
		pSPIHandle->TxLen = length;
		// Mark the SPI state as busy in transmission so that no other code will use
		pSPIHandle->TxState = SPI_BUSY_TX;
		// Enable the TXEIE control bit to get interrupt when TXE
		pSPIHandle->pSPIx->CR2 |= (1 << 7);
		// Data transmission is handled by ISR
	}
	return pSPIHandle->TxState;
}


/**
 * @SPI_RECEIVE_IT
 *
 * Interrupt Enabled SPI RECEIVE Method
 *
 * @param pSPIHandle		The handle for the SPI port to use
 * @param pRxBuffer			The location to read the data to
 * @param length			The length of the data to receive
 */
uint8_t SPI_RECEIVE_IT(SPI_Handle_t *pSPIHandle, uint8_t *pRxBuffer, uint32_t length)
{
	if (pSPIHandle->RxState == SPI_READY) {
		pSPIHandle->pRxBuffer = pRxBuffer;
		pSPIHandle->RxLen = length;
		pSPIHandle->RxState = SPI_BUSY_RX;
		pSPIHandle->pSPIx->CR2 |= (1 << 6);
	}
	return pSPIHandle->RxState;
}

/**
 * @SPI_IRQConfig
 *
 * Configure the Interrupt for the SPI Peripheral
 *
 * @param IRQNumber		The Interrupt Position on the EXTI Controller
 * @param ENorDI		The ENABLE or DISABLE Macro
 */
void SPI_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI)
{
	IRQConfig(IRQNumber, ENorDI);
}

/**
 * @SPI_IRQPriorityConfig
 *
 * Configure the Priority for a given IRQ Number
 *
 * @param IRQNumber		The Interrupt Number to configure
 * @param IRQPriority	The Priority to Configure it to
 */
void SPI_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority)
{
	IRQPriorityConfig(IRQNumber, IRQPriority);
}

/**************************************************************************************************************
 * 													IRQ Handling Functions
 *************************************************************************************************************/

static void spi_txe_interrupt_handle(SPI_Handle_t *pHandle)
{
	// Write to Data Register
	if (pHandle->pSPIx->CR1 & ( 1 << SPI_CR1_DFF) ) {
		// Assigns 2 Bits to DR
		pHandle->pSPIx->DR = *((uint16_t*) pHandle->pTxBuffer);
		pHandle->TxLen --;
		pHandle->TxLen --;
		(uint16_t*)pHandle->pTxBuffer++;
	} else {
		// Assigns 1 Bit to DR
		pHandle->pSPIx->DR = *(pHandle->pTxBuffer);
		pHandle->TxLen --;
		pHandle->pTxBuffer++;
	}

	if (! pHandle->TxLen) {
		// Close the SPI
		pHandle->pSPIx->CR2 &= ~(1 << 7);
		pHandle->pTxBuffer = NULL;
		pHandle->TxLen = 0;
		pHandle->TxState = SPI_READY;
		SPI_ApplicationEventCallback(pHandle, SPI_EVENT_TX_DONE);
	}
}

static void spi_rxne_interrupt_handle(SPI_Handle_t *pHandle)
{
	// Check the DFF
	if(pHandle->pSPIx->CR1 & (1 << SPI_CR1_DFF)) {
		*((uint16_t*)pHandle->pRxBuffer) = pHandle->pSPIx->DR;
		pHandle->RxLen--;
		pHandle->RxLen--;
		(uint16_t*)pHandle->pRxBuffer++;
	} else {
		*(pHandle->pRxBuffer) = pHandle->pSPIx->DR;
		pHandle->RxLen--;
		pHandle->pRxBuffer++;
	}

	if (! pHandle->RxLen) {
		// Close the SPI
		pHandle->pSPIx->CR2 &= ~(1 << 6);
		pHandle->pRxBuffer = NULL;
		pHandle->RxLen = 0;
		pHandle->RxState = SPI_READY;
		SPI_ApplicationEventCallback(pHandle, SPI_EVENT_RX_DONE);
	}
}

static void spi_ovr_interrupt_handle(SPI_Handle_t *pHandle)
{
	// Clear OVR Flag
	uint8_t temp;
	if (pHandle->TxState != SPI_BUSY_TX){
		temp = pHandle->pSPIx->DR;
		temp = pHandle->pSPIx->SR;
	}
	SPI_ApplicationEventCallback(pHandle, SPI_EVENT_OVR_ERR);
}

/**
 * @SPI_IRQHandling
 *
 * Handles the Interrupt
 *
 * @param pHandle		The Handle for a SPIx Peripheral that has called the interrupt
 */
void SPI_IRQHandling(SPI_Handle_t *pHandle)
{
	// Check for TXE
	if ((pHandle->pSPIx->SR & ( 1 << SPI_SR_TXE )) && (pHandle->pSPIx->CR2 & ( 1 << 7 )))
	{
		// Code to handle TXE
		spi_txe_interrupt_handle(pHandle);
	}

	// Check for RXNE
	if ((pHandle->pSPIx->SR & ( 1 << SPI_SR_RXNE )) && (pHandle->pSPIx->CR2 & ( 1 << 6 )))
	{
		// Code to handle RXNE
		spi_rxne_interrupt_handle(pHandle);
	}

	// Check for OVR
	if ((pHandle->pSPIx->SR & ( 1 << SPI_SR_OVR )) && (pHandle->pSPIx->CR2 & ( 1 << 5 )))
	{
		// Code to handle OVR
		spi_ovr_interrupt_handle(pHandle);
	}
}


/**
 * @SPI_PeripheralControl
 *
 * Enable or disable the peripheral
 *
 * @param pSPIx		Base address of peripheral to control
 * @param ENorDI	ENABLE or DISABLE macro
 */
void SPI_PeripheralControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI) {
	if (ENorDI == ENABLE)
		pSPIx->CR1 |= (1 << 6);
	else
		pSPIx->CR1 &= ~(1 << 6);
}


/**
 * @SPI_SSIControl
 *
 * Enable or Disable SSI for a SPIx Peripheral
 *
 * @param pSPIx 		The SPIx Peripheral SSI To Modify
 * @param ENorDI		The ENABLE or DISABLE Macro
 */
void SPI_SSIControl(SPI_RegDef_t *pSPIx, uint8_t ENorDI) {
	if (ENorDI == ENABLE)
		pSPIx->CR1 |= (1 << 8);
	else
		pSPIx->CR1 &= ~(1 << 8);
}


__attribute__((weak)) void SPI_ApplicationEventCallback(SPI_Handle_t *pHandle, uint8_t Event);
