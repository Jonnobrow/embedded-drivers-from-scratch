
#include "stm32f401xx_gpio.h"

/**
 * Initialises a GPIO Pin
 * @param pGPIOHandle	The Handle Structure for a GPIO Pin
 */
void GPIO_Init(GPIO_Handle_t *pGPIOHandle)
{

	GPIO_PCLK_Control(pGPIOHandle->pGPIOx, ENABLE);

	if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode <= GPIO_MODE_ANALOG) {
		pGPIOHandle->pGPIOx->MODER &= ~( 0x3 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		pGPIOHandle->pGPIOx->MODER |= pGPIOHandle->GPIO_PinConfig.GPIO_PinMode << ( 2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	} else {
		if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_FT) {
			EXTI->RTSR &= ~( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			EXTI->FTSR |= ( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		} else if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_RT) {
			EXTI->FTSR &= ~( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			EXTI->RTSR |= ( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		} else if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_IT_RFT) {
			EXTI->FTSR |= ( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			EXTI->RTSR |= ( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
		}

		uint32_t temp = pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber / 4;
		uint32_t temp2 = pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 4;
		SYSCFG_PCLK_EN();
		SYSCFG->EXTICR[temp] |= (GPIO_ADDR_TO_CODE(pGPIOHandle->pGPIOx) << (temp2 * 4));

		// Enable EXTI Interrupt Delivery
		EXTI->IMR |= ( 1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	}

	pGPIOHandle->pGPIOx->OSPEEDR |= pGPIOHandle->GPIO_PinConfig.GPIO_PinSpeed << ( 2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->OSPEEDR &= ~( 0x3 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->PUPDR |= pGPIOHandle->GPIO_PinConfig.GPIO_PinPuPdControl << ( 2 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->PUPDR &= ~( 0x3 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->OTYPER |= pGPIOHandle->GPIO_PinConfig.GPIO_PinOPType << ( pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
	pGPIOHandle->pGPIOx->OTYPER &= ~( 0x1 << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);

	if (pGPIOHandle->GPIO_PinConfig.GPIO_PinMode == GPIO_MODE_ALTFN) {
		if (pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber < 8) {
			pGPIOHandle->pGPIOx->MODER &= ~( 0xF << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			pGPIOHandle->pGPIOx->AFR[1] |= pGPIOHandle->GPIO_PinConfig.GPIO_PinAltFunMode << ( 4 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 8);
		} else {
			pGPIOHandle->pGPIOx->AFR[1] &= ~( 0xF << pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber);
			pGPIOHandle->pGPIOx->AFR[1] |= pGPIOHandle->GPIO_PinConfig.GPIO_PinAltFunMode << ( 4 * pGPIOHandle->GPIO_PinConfig.GPIO_PinNumber % 8);
		}
	}
}

/**
 * Resets the GPIO Port
 * @param pGPIOx	The base address of the GPIO Port
 */
void GPIO_DeInit(GPIO_RegDef_t *pGPIOx)
{
	if (pGPIOx == GPIOA)
	{
		GPIOA_REG_RESET();
	} else if (pGPIOx == GPIOB)
	{
		GPIOB_REG_RESET();
	} else if (pGPIOx == GPIOC)
	{
		GPIOC_REG_RESET();
	} else if (pGPIOx == GPIOD)
	{
		GPIOD_REG_RESET();
	} else if (pGPIOx == GPIOE)
	{
		GPIOE_REG_RESET();
	} else if (pGPIOx == GPIOH)
	{
		GPIOH_REG_RESET();
	}
}

/**
 * Reads data from a specific pin
 * @param pGPIOx		The base address of the port to read
 * @param PinNumber		The pin in the port to read from
 * @return	The data from the pin
 */
uint8_t GPIO_ReadFromInputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber)
{
	return (uint8_t) (pGPIOx->IDR >> PinNumber & 0x00000001);
}

/**
 * Reads data from a specific port
 * @param pGPIOx 		The base address of the port to read
 * @return	The data from the port
 */
uint16_t GPIO_ReadFromInputPort(GPIO_RegDef_t *pGPIOx)
{
	return (uint16_t) (pGPIOx->IDR);
}

/**
 * Write data to a pin
 * @param pGPIOx		The port in which the pin is contained
 * @param PinNumber		The number of the pin within the port
 * @param value			The value to write to the pin
 */
void GPIO_WriteToOutputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t value)
{
	if (value == 1)
	{
		pGPIOx->ODR |= (1 << PinNumber);
	} else {
		pGPIOx->ODR &= ~(1 << PinNumber);
	}
}

/**
 * Write data to a port
 * @param pGPIOx		The base address of the port to write to
 * @param value			The value to write to the port
 */
void GPIO_WriteToOutputPort(GPIO_RegDef_t *pGPIOx, uint16_t value){
	pGPIOx->ODR = value;
}

/**
 * Toggle the output pin for the port
 * @param pGPIOx		The port base address
 * @param PinNumber		The pin to toggle
 */
void GPIO_ToggleOutputPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber){
	pGPIOx->ODR ^= ( 1 << PinNumber );
}

/**
 * Enables or Disables the Peripheral Clock
 * @param pGPIOx	The base address for the GPIO Port
 * @param ENorDI	The ENABLE or DISABLE Macro
 */
void GPIO_PCLK_Control(GPIO_RegDef_t *pGPIOx, uint8_t ENorDI){

	if (ENorDI == ENABLE) {

		if (pGPIOx == GPIOA)
		{
			GPIOA_PCLCK_EN();
		} else if (pGPIOx == GPIOB)
		{
			GPIOB_PCLCK_EN();
		} else if (pGPIOx == GPIOC)
		{
			GPIOC_PCLCK_EN();
		} else if (pGPIOx == GPIOD)
		{
			GPIOD_PCLCK_EN();
		} else if (pGPIOx == GPIOE)
		{
			GPIOE_PCLCK_EN();
		} else if (pGPIOx == GPIOH)
		{
			GPIOH_PCLCK_EN();
		}

	} else {

		if (pGPIOx == GPIOA)
		{
			GPIOA_PCLCK_DI();
		} else if (pGPIOx == GPIOB)
		{
			GPIOB_PCLCK_DI();
		} else if (pGPIOx == GPIOC)
		{
			GPIOC_PCLCK_DI();
		} else if (pGPIOx == GPIOD)
		{
			GPIOD_PCLCK_DI();
		} else if (pGPIOx == GPIOE)
		{
			GPIOE_PCLCK_DI();
		} else if (pGPIOx == GPIOH)
		{
			GPIOH_PCLCK_DI();
		}

	}

}

/**
 * Enable or disable IRQ
 * @param IRQNumber			The IRQ Number to enable or disable
 * @param ENorDI			ENABLE or DISABLE Macro
 */
void GPIO_IRQConfig(uint8_t IRQNumber, uint8_t ENorDI)
{
	IRQConfig(IRQNumber, ENorDI);
}

void GPIO_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority)
{
	IRQPriorityConfig(IRQNumber, IRQPriority);
}

void GPIO_IRQHandling(uint8_t PinNumber)
{
	if(EXTI->PR & (1 << PinNumber))
		EXTI->PR |= ( 1 << PinNumber);
}
