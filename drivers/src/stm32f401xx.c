/*
 * stm32f401xx.c
 *
 *  Created on: 11 Jul 2019
 *      Author: jonathan.bartlett
 */

#include "stm32f401xx.h"

void IRQConfig(uint8_t IRQNumber, uint8_t ENorDI)
{
	if(ENorDI == ENABLE) {
		if (IRQNumber <= 31) {
			// ISER0
			*NVIC_ISER0 |= (1 << IRQNumber);
		} else if (IRQNumber > 31 && IRQNumber < 64) {
			// ISER1
			*NVIC_ISER1 |= (1 << IRQNumber % 32);
		} else if (IRQNumber >= 64 && IRQNumber < 96) {
			// ISER2
			*NVIC_ISER2 |= (1 << IRQNumber % 64);
		}
	} else {
		if (IRQNumber <= 31) {
			// ICER0
			*NVIC_ICER0 |= (1 << IRQNumber);
		} else if (IRQNumber > 31 && IRQNumber < 64) {
			// ICER1
			*NVIC_ICER1 |= (1 << IRQNumber % 32);
		} else if (IRQNumber >= 64 && IRQNumber < 96) {
			// ICER2
			*NVIC_ICER2 |= (1 << IRQNumber % 64);
		}
	}
}

void IRQPriorityConfig(uint8_t IRQNumber, uint8_t IRQPriority)
{
	uint8_t iprx = IRQNumber / 4;
	uint8_t sect = IRQNumber % 4;

	uint8_t shift_amount = (8 * sect) + (8 - NUM_PR_BIT_IMPLEMENTED);
	*(NVIC_PRBASE + (iprx)) |= (IRQPriority << shift_amount);
}
