/*
 * 004_spi_tx_testing.c

 *
 *  Created on: 11 Jul 2019
 *      Author: jonathan.bartlett
 */

#include <string.h>
#include "stm32f401xx_spi.h"
#include "stm32f401xx_gpio.h"

/*
 * PB9 			SPI2_NSS
 * PB10			SPI2_SCLK
 * PB14			SPI2_MISO
 * PB15			SPI2_MOSI
 * All using the AF5 Mode
 */

void SPI_GPIOInit(void)
{
	GPIO_Handle_t SPIPins;
	SPIPins.pGPIOx = GPIOB;
	SPIPins.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_ALTFN;
	SPIPins.GPIO_PinConfig.GPIO_PinAltFunMode = 5;
	SPIPins.GPIO_PinConfig.GPIO_PinOPType = GPIO_OTYPE_PP;
	SPIPins.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PUPDR_NO;
	SPIPins.GPIO_PinConfig.GPIO_PinSpeed = GPIO_OSPEED_VH;

	// Clock
	//SPIPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_9;
	//GPIO_Init(&SPIPins);
	SPIPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_10;
	GPIO_Init(&SPIPins);
	//SPIPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_14;
	//GPIO_Init(&SPIPins);
	SPIPins.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_15;
	GPIO_Init(&SPIPins);

}

void SPI2_Init(void)
{
	SPI_Handle_t SPI2Handle;
	SPI2Handle.pSPIx = SPI2;

	SPI2Handle.SPIConfig.SPI_BusConfig = SPI_BUS_CONFIG_FD;
	SPI2Handle.SPIConfig.SPI_DeviceMode = SPI_DEVICE_MODE_MASTER;
	SPI2Handle.SPIConfig.SPI_SclkSpeed = SPI_BAUD_PCLK_2;
	SPI2Handle.SPIConfig.SPI_DFF = SPI_DFF_BITS_8;
	SPI2Handle.SPIConfig.SPI_CPHA = SPI_CPHA_LOW;
	SPI2Handle.SPIConfig.SPI_CPOL = SPI_CPOL_LOW;
	SPI2Handle.SPIConfig.SPI_SSM = SPI_SSM_ENABLE;

	SPI_INIT(&SPI2Handle);
}

int main(void) {

	SPI_GPIOInit();
	SPI2_Init();

	SPI_SSIConfig(SPI2, ENABLE);

	SPI_PeripheralControl(SPI2, ENABLE);

	char data[] = "Hello World!";

	SPI_SEND(SPI2, (uint8_t*)data, strlen(data));

	SPI_PeripheralControl(SPI2, DISABLE);

	while(1);

	return 0;
}
