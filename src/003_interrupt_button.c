/*
 * 003_interrupt_button.c
 *
 *  Created on: 10 Jul 2019
 *      Author: jonathan.bartlett
 */

#include "stm32f401xx.h"
#include "stm32f401xx_gpio.h"

void delay(void) {
	for (uint32_t i = 0; i < 500000; i ++);
}

int main(void){

	GPIO_Handle_t GPIOLed;
	GPIOLed.pGPIOx = GPIOA;
	GPIOLed.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_5;
	GPIOLed.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
	GPIOLed.GPIO_PinConfig.GPIO_PinSpeed = GPIO_OSPEED_H;
	GPIOLed.GPIO_PinConfig.GPIO_PinOPType = GPIO_OTYPE_PP;
	GPIOLed.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PUPDR_NO;

	GPIO_Handle_t GPIOButton;
	GPIOButton.pGPIOx = GPIOC;
	GPIOButton.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_13;
	GPIOButton.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_IT_FT;
	GPIOButton.GPIO_PinConfig.GPIO_PinSpeed = GPIO_OSPEED_H;
	GPIOButton.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PUPDR_NO;

	GPIO_PCLK_Control(GPIOA, ENABLE);
	GPIO_PCLK_Control(GPIOC, ENABLE);
	GPIO_Init(&GPIOLed);
	GPIO_Init(&GPIOButton);

	GPIO_IRQConfig(IRQ_NO_EXTI15_10,ENABLE);
	GPIO_IRQPriorityConfig(IRQ_NO_EXTI15_10, 47);

	while(1);

}

void EXTI15_10_IRQHandler(void)
{
	GPIO_IRQHandling(13);
	GPIO_ToggleOutputPin(GPIOA, 5);
}
