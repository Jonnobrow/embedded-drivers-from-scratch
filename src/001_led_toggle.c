#include "stm32f401xx.h"
#include "stm32f401xx_gpio.h"

void delay(void) {
	for (uint32_t i = 0; i < 500000; i ++);
}

int main(void){

	GPIO_Handle_t GPIOLed;
	GPIOLed.pGPIOx = GPIOA;
	GPIOLed.GPIO_PinConfig.GPIO_PinNumber = GPIO_PIN_NO_5;
	GPIOLed.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_OUT;
	GPIOLed.GPIO_PinConfig.GPIO_PinSpeed = GPIO_OSPEED_H;
	GPIOLed.GPIO_PinConfig.GPIO_PinOPType = GPIO_OTYPE_PP;
	GPIOLed.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PUPDR_NO;

	GPIO_PCLK_Control(GPIOA, ENABLE);
	GPIO_Init(&GPIOLed);

	while(1) {
		GPIO_ToggleOutputPin(GPIOA, GPIO_PIN_NO_5);
		delay();
	}
}
